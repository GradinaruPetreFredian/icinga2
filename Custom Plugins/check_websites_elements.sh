#!/bin/bash

# Description:  Custom Nagios/Icinga2 script for checking elements returned by some custom websites .
# Author:       Petre Fredian Gradinaru, RO
# Date:         26 of April 2021

#Variables and defaults
STATE_OK=0;             # define the exit code if status is OK
STATE_CRITICAL=2;       # define the exit code if status is Critical
STATE_UNKNOWN=3;        # define the exit code if status is Unknown


help="check_elements.sh\n
Usage: $0 -U [Url] -E [Element] -C [Critical]\n
\nOptions:\n
\t-U Url\n
\t-E Element Name\n
\t-C Critical Treeshold\n
\n"

if [ "${1}" = "--help" -o "${#}" = "0" ];
       then echo -e ${help}; exit 1;
fi
#########################################################################
# Get user-given variables
while getopts "U:E:C:h" Input;
do
  case ${Input} in
  U)      url=${OPTARG};;
  E)      element=${OPTARG};;
  C)      critical=${OPTARG};;
  h)      echo -e ${help}; exit ${STATE_UNKNOWN};;
  *)      echo -e ${help}; exit ${STATE_UNKNOWN};;
  esac
done
curloption="";          # define custom curl option for https websites

# Check if it's a secured website
string=$(echo $url | cut -c1-5)
if [[ "$string" == "https" ]]; then
    curloption="-k"
fi

# Run command
if [ $element == "all-stores-count" ]
   then
     command="$(curl -s $curloption $url -i | grep $element)";
     output=$command;
     # Get the number of elements from the output of the command
     number=$(echo $output | cut -d '>' -f2 | sed -e 's/......$//');
   elif [ $element == "result-found" ]
    then
      command="$(curl -s $curloption $url -i | grep $element)";
      output=$command;
      # Get the number of elements from the output of the command
      number=$(echo $output | cut -d '"' -f4);
   else
      echo "UNKNOWN - You entered an element which was not integrated in the check-plugin or it is not present on the website !";
      exit $STATE_UNKNOWN;
fi

# Get the results and provide them for Icinga2
if [ $number -gt $critical ]
   then
      echo "OK - The number of ${element} are ${number} on the website ${url} .";
      exit $STATE_OK;
   else
      echo "CRITICAL - The number of ${element} are ${number} on the website ${url} .";
      exit $STATE_CRITICAL;
fi
