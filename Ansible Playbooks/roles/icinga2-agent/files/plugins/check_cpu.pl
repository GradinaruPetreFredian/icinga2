#!/usr/bin/perl

#
#  check_cpu.pl
#       Purpose: This script calculates the cpu-usage in % from the output of top command (top -bn1)
#       Input:   Warning, Critical Threshold in %
#
#


use strict;
use Getopt::Std;

#define variables
use vars qw($opt_h $opt_w $opt_c);

my $units="";
my $warning="";
my $critical="";
my $scriptname="check_disk.pl";
my $text="CPU OK - ";
my $output="";
my $rc=0;
my $cpu_idle=0;
my $cpu_usage=0;
my $perfdata="";

# Get the options and check the values
init();


# run top-command and fetch result    will take 2 samples and get result of last sample. Using only 1 sample shows inaccurate values.
$output=`/usr/bin/top -bn2 | /usr/bin/grep "Cpu(s)" | /usr/bin/tail -n 1`;
chomp($output);

($cpu_idle)=($output=~/.*?(\d+\.\d+).id/);
if ($cpu_idle == 100) {                                # avoid value "0.0"
   $cpu_usage=0;
} else {
   $cpu_usage=sprintf("%.1f",100-$cpu_idle);
};

if ( $cpu_usage >= $critical ) {
   $text="CPU CRITICAL - ";
   $rc=2;
};

if ( $cpu_usage >= $warning ) {
   $text="CPU WARNING - ";
   $rc=1;
};

$text.="Usage: ${cpu_usage}% ";

$perfdata="cpu_usage=${cpu_usage}%;$warning;$critical;0;100";


# Print Result
print "${text} | $perfdata\n";
exit $rc;



#
#  Subroutines
#


# Show usage
sub usage() {
  print "Usage:\n";
  print "$scriptname -w WarningThreshold in % -c Critical Threshold in % [-h] \n\n";
  print "       -w: Warning Threshold in % (Warning is triggered if usage is above threshold)\n";
  print "       -c: Critical Threshold in % (Critical Alarm is triggered if usage is above threshold)\n";
exit 3;
}



#
# Get Arguments, store in variables and check values for "sanity"
#
sub init{

  if ($#ARGV le 0) {
    print "No arguments given!\n";
    &usage;
  }
  else {
    getopt('hw:c:');
  }

  if ($opt_h) {
    &usage;
  };
  if ($opt_w) {
    $warning=$opt_w;
    $warning=~s/%//;          # remove % from threshold value
  } else {
    print "You must provide a warning threshold !\n";
    &usage;
  };
  if ($opt_c) {
    $critical=$opt_c;
    $critical=~s/%//;         # remove % from threshold value
  } else {
    print "You must provide a critical threshold !\n";
    &usage;
  };
  if ( $opt_c && $opt_w) {    # check sanity of thresholds
     if ($opt_c < $opt_w) {
        print "Error: Warning Threshold must be smaller or equal to critical Threshold!\n";
        print "Provided Values: Warning: $warning, Critical: $critical\n";
        &usage;
     };
  };

};
