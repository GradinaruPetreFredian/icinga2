#!/usr/bin/perl

#
#  check_disk.pl
#       Purpose: This script is a wrapper around check_disk
#                Alarming thresholds for check_disk will be determined depending on disk size. The pre-defined
#                thresholdy are provided in a config file "check_disk.ini".
#                The Disk Name and determined thresholds are passed to check_disk. It's output will be returned.
#                Meaning of contents of check_disk.ini:
#                    0GB <= Disk size <  100GB --> Warning threshold (used space) = 80%, Critical threshold (used space) = 90%  -> 0;100;80;90
#                  100GB <= Disk size < 1000GB --> Warning threshold (used space) = 90%, Critical threshold (used space) = 95%   -> 100;1000;90;95
#                 1000GB <= Disk size          --> Warning threshold (used space) = 98%, Critical threshold (used space) = 99%    -> 1000;999999;98;99
#      Input:   Disk Name
#               Optional: Warning, Critical Threshold in %
#               If no Warning/Critical Thresholds are provided, the values are auto-calculated from the config file check_disk.ini,
#               otherwise the parameters will overwrite the values of check_disk.ini
#      Output: Extended Output of check_disk. Check_disk only shows free space (absolute and in percent)
#                We will show total/used/free space both absolute and in percent.
#                Example Output of check_disk :    DISK OK - free space: / 8 GB (39% inode=75%);
#                Example Output of this script:    DISK OK - /: Total: 23 GB - Used 15GB (61%,inode=75%) - Free: 8 GB (39%)
#
#        ATTENTION: the capacity values above must not have a "gap". for e.g. first line is from 0 to 100GB --> second line must start with 100GB.
#                   otherwise default thresholds are used (warning: 80%, critical: 90%)
#                   All threshold values MUST be provided in GB (Gigabytes)!
#
#


use strict;
use Getopt::Std;

#define variables
use vars qw($opt_h $opt_w $opt_c $opt_p $opt_u);

my $units="";
my $warning="";
my $critical="";
my $warning_absolute=0;
my $critical_absolute=0;
my $disk_warning="";
my $disk_critical="";
my $disk_units="";
my $perfdata="";
my $path="";
my $scriptname="check_disk.pl";
my $text="DISK OK - ";
my $free_mb=0;
my $used_mb=0;
my $total_mb=0;
my $used=0;
my $total=0;
my $free=0;
my $used_percent=0;
my $free_percent=0;
my $free_inode_percent=0;
my $output="";
my $rc=0;
my %dirs=("/usr/lib/nagios/plugins/","empty","/usr/lib64/nagios/plugins/","empty");
my $command="check_disk";
my $plugin_dir="none";
my $check_command="";

# Get the options and check the values
init();


$disk_warning=100-$warning;
$disk_critical=100-$critical;
$disk_units="MB";                   # do check with unit: MB and calculate lateron GB (otherwise we will have errors in round-operation). check_disk prunes floating point values.



# run check_disk and fetch result
foreach my $dir (keys %dirs){
  $check_command=$dir.$command;
  if ( -f  $check_command) { $plugin_dir=$dir; };
};
if ( $plugin_dir eq "none" ) { die "Error identifiying the icinga plugin directory!"; };
$output=`$plugin_dir/check_disk -w $disk_warning -c $disk_critical -p $path -u $disk_units`;

$output=~s/.*free space: .*? //;                     # remove String ".* free space: [mount-point "
($free_mb)=($output=~/(\d+)/);

$output=~s/.*inode=//;                               # remove String ".*inode="
($free_inode_percent)=($output=~/(\d+)/);

$output=~s/.*;//;                                    # remove String ".*;"
($total_mb)=($output=~/(\d+)/);

$free_percent=sprintf("%.f",($free_mb*100)/$total_mb);
$used_percent=100-$free_percent;


if ($units eq "MB" ) {
   $free=$free_mb;
   $total=$total_mb;
} elsif ($units eq "GB" ) {
   $free=sprintf("%.1f",$free_mb/1024);
   $total=sprintf("%.1f",$total_mb/1024);
} elsif ($units eq "TB" ) {
   $free=sprintf("%.1f",$free_mb/(1024*1024));
   $total=sprintf("%.1f",$total_mb/(1024*1024));
} else {                                          # default is "MB"
  $free=$free_mb;
  $total=$total_mb;
};


$used=$total-$free;

# remove trailing "0": for e.g. 40GB instead of 40.0GB
$free=$total-$used;
$total=$free+$used;


#
#  Auto-detect Thresholds if not both critical AND warning thresholds are provided
#
if ( ($warning eq "auto") || ($critical eq "auto") ){
  my $total_gb=sprintf("%d",$total_mb/1024);
  my $iniFile=$plugin_dir."check_disk.ini";
  my $line="";
  my $lowDisk=0;
  my $highDisk=0;
  my $warn=0;
  my $crit=0;
  open INI, "<$iniFile" or die "Could not open iniFile $iniFile for reading. !\n";
    while ($line=<INI>){
       chomp($line);
       if ( !($line eq "") && !($line=~/^#/) ){          # ignore empty lines and comment lines (starting with "#")
          ($lowDisk,$highDisk,$warn,$crit)=split(/;/,$line);
          if ( ($total_gb >= $lowDisk) && ($total_gb < $highDisk) ) {
             $warning=$warn;
             $critical=$crit;
          };
       };
    };

  close INI;
};


if ( $used_percent >= $critical ) {
   $text="DISK CRITICAL - ";
   $rc=2;
};

if ( $used_percent >= $warning && $used_percent < $critical ) {
   $text="DISK WARNING - ";
   $rc=1;
};



$warning_absolute=sprintf("%.1f",($warning*$total)/100);
$critical_absolute=sprintf("%.1f",($critical*$total)/100);
$perfdata="$path=${used}$units;${warning_absolute};${critical_absolute};0;${total}";
$text.="${path}: Total: $total $units - Used: $used $units (${used_percent}%) - Free: $free $units (${free_percent}%,inode=${free_inode_percent}%)";


# Print Result
print "${text} | $perfdata\n";
exit $rc;



#
#  Subroutines
#


# Show usage
sub usage() {
  print "Usage:\n";
  print "$scriptname -p Mount-Path [-u Units] [-w WarningThreshold in %] [-c Critical Threshold in %] [-h] \n\n";
  print "       -p: Absolute Mount-Path to be checked\n";
  print "       -u: (optional) Units. Default: GB, possible Values: bytes,KB,MB,GB,TB\n";
  print "       -w: (optional) Warning Threshold in % (Warning is triggered if usage is above threshold)\n";
  print "       -c: (optional) Critical Threshold in % (Critical Alarm is triggered if usage is above threshold)\n";
exit 3;
}



#
# Get Arguments, store in variables and check values for "sanity"
#
sub init{

  if ($#ARGV le 0) {
    print "No arguments given!\n";
    &usage;
  }
  else {
    getopt('hw:c:p:u:');
  }

  if ($opt_h) {
    &usage;
  };
  if ($opt_p) {
    $path=$opt_p;
  } else {
    print "You must provide a mount-path to be checked!\n";
    &usage;
  };
  if ($opt_u) {
    $units=$opt_u;
  } else {
    $units="GB";              # default value for units: GB
  };
  if ($opt_w) {
    $warning=$opt_w;
    $warning=~s/%//;          # remove % from threshold value
  } else {
    $warning="auto";          # default: auto-determine warning-threshold
  };
  if ($opt_c) {
    $critical=$opt_c;
    $critical=~s/%//;         # remove % from threshold value
  } else {
    $critical="auto";         # default: auto-determine critical-threshold
  };
  if ( $opt_c && $opt_w) {    # check sanity of thresholds
     if ($opt_c < $opt_w) {
        print "Error: Warning Threshold must be smaller or equal to critical Threshold!\n";
        print "Provided Values: Warning: $warning, Critical: $critical\n";
        &usage;
     };
  };

};
