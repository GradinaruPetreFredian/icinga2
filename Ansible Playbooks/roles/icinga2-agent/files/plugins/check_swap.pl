#!/usr/bin/perl

#
#  check_swap.pl
#       Purpose: This script is a wrapper around check_swap
#      Input:   Warning, Critical Threshold in %
#      Output: Extended Output of check_swap. Check_swap only shows free space (absolute and in percent)
#                We will show total/used/free space both absolute and in percent.
#                Example Output of check_disk :    SWAP OK - 100% free (2043 MB out of 2043 MB)
#                Example Output of this script:    SWAP OK - Total: 2043MB - Used 0MB (0%) - Free: 2043MB (100%)
#
#
#


use strict;
use Getopt::Std;

#define variables
use vars qw($opt_h $opt_w $opt_c $opt_u);

my $units="";
my $warning="";
my $critical="";
my $warning_absolute=0;
my $critical_absolute=0;
my $swap_warning="";
my $swap_critical="";
my $disk_units="";
my $critical="";
my $perfdata="";
my $path="";
my $scriptname="check_swap.pl";
my $text="SWAP OK - ";
my $free_mb=0;
my $used_mb=0;
my $total_mb=0;
my $used=0;
my $total=0;
my $free=0;
my $used_percent=0;
my $free_percent=0;
my $output="";
my $rc=0;
my %dirs=("/usr/lib/nagios/plugins/","empty","/usr/lib64/nagios/plugins/","empty");
my $command="check_swap";
my $plugin_dir="none";
my $check_command="";
my $units="MB";

# Get the options and check the values
init();


$swap_warning=100-$warning;
$swap_critical=100-$critical;


# run check_swap and fetch result
foreach my $dir (keys %dirs){
  $check_command=$dir.$command;
  if ( -f  $check_command) { $plugin_dir=$dir; };
};
if ( $plugin_dir eq "none" ) { die "Error identifiying the icinga plugin directory!"; };

$output=`$plugin_dir/check_swap -w $swap_warning -c $swap_critical`;

($free_mb)=($output=~/\((\d+)/);
($total_mb)=($output=~/out of (\d+)/);
$used_mb=$total_mb - $free_mb;

$free_percent=sprintf("%.f",($free_mb*100)/$total_mb);
$used_percent=100-$free_percent;


#if ($units eq "MB" ) {
#   $free=$free_mb;
#   $total=$total_mb;
#} elsif ($units eq "GB" ) {
#   $free=sprintf("%.1f",$free_mb/1024);
#   $total=sprintf("%d",$total_mb/1024);
#} elsif ($units eq "TB" ) {
#   $free=sprintf("%.1f",$free_mb/(1024*1024));
#   $total=sprintf("%d",$total_mb/(1024*1024));
#} else {                                          # default is "MB"
#  $free=$free_mb;
#  $total=$total_mb;
#};

$total=$total_mb;
$free=$free_mb;
$used=$total-$free;



if ( $used_percent >= $warning ) {
   $text="SWAP WARNING - ";
   $rc=1;
};



if ( $used_percent >= $critical ) {
   $text="SWAP CRITICAL - ";
   $rc=2;
};



$warning_absolute=sprintf("%.1f",($warning*$total)/100);
$critical_absolute=sprintf("%.1f",($critical*$total)/100);
$perfdata="swap=${used}$units;${warning_absolute};${critical_absolute};0;${total}";
$text.="Total: $total $units - Used: $used $units (${used_percent}%) - Free: $free $units (${free_percent}%)";


# Print Result
print "${text} | $perfdata\n";
exit $rc;



#
#  Subroutines
#


# Show usage
sub usage() {
  print "Usage:\n";
  print "$scriptname -w WarningThreshold in % -c Critical Threshold in % [-h] \n\n";
  print "       -w: (optional) Warning Threshold in % (Warning is triggered if usage is above threshold)\n";
  print "       -c: (optional) Critical Threshold in % (Critical Alarm is triggered if usage is above threshold)\n";
exit 3;
}



#
# Get Arguments, store in variables and check values for "sanity"
#
sub init{

  if ($#ARGV le 0) {
    print "No arguments given!\n";
    &usage;
  }
  else {
    getopt('hw:c:');
  }

  if ($opt_h) {
    &usage;
  };
  if ($opt_w) {
    $warning=$opt_w;
    $warning=~s/%//;          # remove % from threshold value
  } else {
    $warning="auto";          # default: auto-determine warning-threshold
  };
  if ($opt_c) {
    $critical=$opt_c;
    $critical=~s/%//;         # remove % from threshold value
  } else {
    $critical="auto";         # default: auto-determine critical-threshold
  };
  if ( $opt_c && $opt_w) {    # check sanity of thresholds
     if ($opt_c < $opt_w) {
        print "Error: Warning Threshold must be smaller or equal to critical Threshold!\n";
        print "Provided Values: Warning: $warning, Critical: $critical\n";
        &usage;
     };
  };

};
