#
#  check_service.sh
#
#  checks for service state ("active", "reloading", "down", ..)  via systemctl
#


# Nagios return codes
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

RC=0

print_usage() {
        echo "Usage: check_service_state.sh [options]"
        echo ""
        echo "Options:"
        echo "  -s  <string> : Service Name"
        echo "  -h  Show this page"
        echo ""
}

print_help() {
        print_usage
        echo ""
        echo "This plugin will check the State of a Service via command systemctl."
        echo ""
        exit 0
}


# Parse parameters
while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)
            print_help
            exit $STATE_OK
             ;;
        -s | --service)
            shift
           SERVICE=$1
             ;;
        *)  echo "Unknown argument: $1"
            print_usage
        esac
shift
done


SERVICESTATE=`/usr/bin/systemctl is-active $SERVICE`
if [ $SERVICESTATE == "active" ]; then
    OUTPUT="OK - Service $SERVICE is ${SERVICESTATE}."
    RC=0;
 else
    OUTPUT="CRITICAL - Service $SERVICE is ${SERVICESTATE}."
    RC=2;
fi

echo $OUTPUT
exit $RC
