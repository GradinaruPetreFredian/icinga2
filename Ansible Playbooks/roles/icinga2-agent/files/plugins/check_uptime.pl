#!/usr/bin/perl

#
#  check_uptime.pl
#       Purpose: This script is a wrapper around /bin/uptime
#       Input:   none
#       Output:  Modified Output of /bin/uptime + perfdata
#
#


use strict;
use Getopt::Std;

#define variables
use vars qw($opt_h);

my $output="";
my $rc=0;
my $days=0;
my $minutes=0;
my $hours=0;
my $perfdata="";
my $scriptname="check_uptime.pl";
my $text="";
my $uptime_minutes=0;

# Get the options and check the values
init();



# run check_disk and fetch result
$output=`/usr/bin/uptime`;          # example output: " 10:02am  up 16 days 17:37,  5 users,  load average: 0.00, 0.00, 0.00"
chomp($output);

$output=~s/.*up //;                     # remove time
if ( $output=~/days/ ){
  ($days)=($output=~/^(\d+) /);
  $output=~s/\d+ days.//;                               # remove days
};
($hours,$minutes)=($output=~/(\d+):(\d+),/);

$uptime_minutes=$days*24*60+$hours*60+$minutes;
$perfdata="uptime_min=$uptime_minutes";

$text="OK - Uptime is ${days}d ${hours}h ${minutes}min";


# Print Result
print "${text} | $perfdata\n";
exit $rc;



#
#  Subroutines
#


# Show usage
sub usage() {
  print "Usage:\n";
  print "$scriptname [-h] \n\n";
  print "       Prints output of /bin/uptime, calculates uptime in minutes and adds this as perfdata to the output.\n";
exit 3;
}



#
# Get Arguments, store in variables and check values for "sanity"
#
sub init{

  if ($#ARGV gt 0) {
    getopt('h');
  }

  if ($opt_h) {
    &usage;
  };
};
